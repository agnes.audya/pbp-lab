from django.http import response
from django.shortcuts import render
from .models import Note
from django.http.response import HttpResponse
from django.core import serializers

# Create your views here.

def index(request):
    response = {'notes':Note.objects.all()}
    return render(request, 'lab2.html', response)

def xml(request):
    response = Note.objects.all()
    response = serializers.serialize('xml', response)
    return HttpResponse(response, content_type="application/xml")

def json(request):
    response = Note.objects.all()
    response = serializers.serialize('json', response)
    return HttpResponse(response, content_type="application/json")