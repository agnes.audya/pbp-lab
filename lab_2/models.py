from django.db import models

# Create your models here.
class Note(models.Model):
    to = models.CharField("To", max_length=50)
    dari = models.CharField("From", max_length=50)
    title = models.CharField("Title",max_length=100)
    message = models.TextField("Message")

