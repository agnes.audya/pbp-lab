import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: "Belajar Form Flutter",
    home: BelajarForm(),
  ));
}

class User {
  String name;

  User(this.name);
}

class BelajarForm extends StatefulWidget {
  @override
  _BelajarFormState createState() => _BelajarFormState();
}

class _BelajarFormState extends State<BelajarForm> {
  final _formKey = GlobalKey<FormState>();

  // double nilaiSlider = 1;
  // bool nilaiCheckBox = false;
  // bool nilaiSwitch = true;
  final myController = TextEditingController();
  void Hello(String value) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => SecondRoute(user: User(value))),
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
          appBarTheme: AppBarTheme(
        color: const Color(0xFF151026),
      )),
      home: Scaffold(
        appBar: AppBar(
          title: Text("Rumah Sakit"),
        ),
        body: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.all(20.0),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      controller: myController,
                      decoration: new InputDecoration(
                        hintText: "contoh: Susilo Bambang",
                        labelText: "Nama Lengkap",
                        border: OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(5.0)),
                      ),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Nama tidak boleh kosong';
                        }
                        return null;
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      obscureText: true,
                      decoration: new InputDecoration(
                        hintText: "contoh: XX23Jsds",
                        labelText: "Password",
                        border: OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(5.0)),
                      ),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Password tidak boleh kosong';
                        }
                        return null;
                      },
                    ),
                  ),
                  RaisedButton(
                    child: Text(
                      "Submit",
                      style: TextStyle(color: Colors.amber),
                    ),
                    color: Colors.green,
                    onPressed: () {
                      if (_formKey.currentState!.validate()) {
                        Hello(myController.text);
                      }
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class SecondRoute extends StatelessWidget {
  const SecondRoute({Key? key, required this.user}) : super(key: key);
  final User user;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Second Route"),
      ),
      body: Center(
        child: Text("Hello " + user.name),
      ),
    );
  }
}
