import 'package:flutter/material.dart';
import 'package:flutter_complete_guide/models/category.dart';
import 'package:flutter_complete_guide/widgets/category_item.dart';
import 'package:flutter_complete_guide/widgets/main_drawer.dart';

class helloScreen extends StatelessWidget {
  // static const routeName = '/Welcome';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Layanan Rumah Sakit'),
      ),
      drawer: MainDrawer(),
      body: const Center(
        child: Text("Selamat Datang", style: TextStyle(fontSize: 100)),
      ),
    );
  }
}
