import 'package:flutter/material.dart';
import './models/category.dart';

const DUMMY_CATEGORIES = const [
  Category(
    id: 'c1',
    title: 'RS Venus',
    color: Colors.purple,
  ),
  Category(
    id: 'c1',
    title: 'RS Jupiter',
    color: Colors.red,
  ),
];
