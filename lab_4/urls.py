from django.urls import path
from lab_4.views import index, add_note, note_list


urlpatterns = [
    path('', index, name='index'),
    path('add-note/', add_note, name='addNote'),
    path('note-list/', note_list, name='noteList')
]