## 1. Perbedaan antara JSON dan XML

- {+ JSON disimpan dalam bentuk key:value +}
- [+ JSON tidak memiliki endtag. +]
- [+ JSON hanya menampilkan data. +]
- {- XML memiliki start tag dan harus memiliki closing tag. -}
- {- XML dapat memiliki styling seperti font. -}
- {- XML digunakan untuk mengirim data. -}


## 2. Perbedaan antara XML dan HTML
- {+ HTML berfokus kepada bagaimana menampilkan konten +}
- {+ HTML memiliki keterbatasan dalam tags +}
- {+ HTML memiliki tags yang case insensitive +}
- {- XML berfokus untuk menyimpan atau mengirim data -} - 
- {- XML memiliki tags yang lebih fleksibel -} - 
- {- XML memiliki tags yang case sensitive -} - 
