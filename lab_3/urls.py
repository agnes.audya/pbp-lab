from django.urls import path
from lab_3.views import index, add_friend
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('', index, name='index'),
    path('admin/login/', auth_views.LoginView.as_view()),
    path('add/', add_friend, name='addFriend')
]