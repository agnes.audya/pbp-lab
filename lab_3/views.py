from django.shortcuts import render
from .forms import FriendForm
from django.http import HttpResponseRedirect
from lab_1.views import Friend
from django.contrib.auth.decorators import login_required


@login_required(login_url='/admin/login/')
def index(request):
    response = {'friends':Friend.objects.all()}
    return render(request, 'lab3_index.html', response)


def add_friend(request):
    form = FriendForm(request.POST or None)

    if(form.is_valid() and request.method == 'POST'):
        form.save()
        return HttpResponseRedirect('/lab-3')

    return render(request, 'lab3_form.html', {'form': form})
      

